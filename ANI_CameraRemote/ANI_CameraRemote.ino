/*--------------------------------------------------
  ANI Camera Remote by Andreas Niggemann, 2018
  Versions:
  20170607 Base functions
  20170613 Shot counter for WebGUI
  20170712 Defining triggerPin, settings
  20170717 Refactoring
  20170720 Bugfix: Restarting timelapse after reconnect removed
  20170720 Information is displayed all the time, refresh button
  20170720 Autorefresh setting added
  20170722 Input pin as timelapse start
  20170724 Setting for autostart timelapse mode
  20170724 Calculate remaining time
  20180506 Preparatory work for M5Stack (display and keyboard)
  20180506 Hint: Use ESP8266 V2.4.0 since V2.4.1 has problems
  20180510 First Version for M5Stack
  20180511 M5Stack: WiFi OFF by default, config menu, C button as value accelerator
  20180528 Setting for trigger duration
  20180529 Preparatory work for timetable
  20180601 Preparatory work for autofocus pin
  20180603 More compact font
  20180603 Show trigger state ON by flashing screen
  20180614 Refactoring
  20180621 Preparatory work for autostart file from SD card
  20180623 Autostart timetable from SD Card for M5Stack and ESP8266
  20180623 Shutter sound
  20180623 Serial output including verbosity
  20180624 Load configuration from microSD card file
  20180624 Timetable elements L,H,I
  20180624 Refactoring
  20180625 Uploading files to microSD card
  20180628 Buffering serial output at startup
  20180628 Delete file via URL
  20180629 Execute timetable from file on the microSD card
  --------------------------------------------------*/
#ifdef ESP32
#include <M5Stack.h>
#include <WiFi.h>
#include <Free_Fonts.h>
#define SWITCH_WIFI_ON false
#else
#include <ESP8266WiFi.h>
#define SWITCH_WIFI_ON true
#endif

#include <SPI.h>
#include <SD.h>

//--------------------------------------------------
// Settings:
//
String ssid = "ANI-CameraRemote";            // WiFi SSID
String password = "Remoter12345678";         // set to "" for open access point w/o password
int webServerPort = 80;                      // Port for web server

int screenSaverTime = 60;                    // M5Stack: Switch off screen after 60 seconds of inactivity
int shutterSoundFrequency = 0;               // M5Stack: Frequency for trigger sound, 0 = sound OFF
int serialSpeed = 115200;                    // Speed for serial output
int serialVerbosity = 2;                     // Verbosity 0 = no output, 1 = output important things, 2 = all

int triggerPin = 2;                          // GPIO2 as trigger output
int autofocusPin = 0;                        // GPIO0 as autofocus pin, -1 = deactivate autofocus start via pin
int startPin = 5;                            // GPIO5 as start input for timelapse, -1 = deactivate input pin processing
int microSDCardChipSelect = 4;               // GPIO4 as chip select for the microSD card

// Timelapse Defaults
unsigned long default_delayToStart = 0;      // Delay in seconds till start of timelapse
unsigned long default_numberOfShots = 10;    // Number of shots in timelapse mode
unsigned long default_delayBetweenShots = 5; // Delay between shots in timelapse mode
unsigned long default_triggerDuration = 0;   // Duration of trigger ON in seconds (0 = 250ms)
unsigned long default_autorefresh = 5;       // In timelapse mode autorefresh webGUI every 5 seconds, 0 = autorefresh off
int timelapseAutoStart = 0;                  // 1 = Autostart timelapse mode, 0 = No autostart

// End of settings
//--------------------------------------------------



// Global variables
const String prgTitle = "ANI Camera Remote";
const String prgVersion = "1.6";
const String screenHeaderLine = prgTitle + " " + prgVersion;
const int versionMonth = 6;
const int versionYear = 2018;

const int timeSlot = 250; // timeslot in ms
const int timeSlotsPerSecond = 1000 / timeSlot;

const int flushSerialBuffer = 1000;

String timeTable = "";
const String timeTableSeparator = ",";

unsigned long delayToStart = default_delayToStart;
unsigned long numberOfShots = default_numberOfShots;
unsigned long delayBetweenShots = default_delayBetweenShots;
unsigned long triggerDuration = default_triggerDuration;
unsigned long autorefresh = default_autorefresh;
unsigned long previousTimeSlotMillis = 0;
unsigned long currentDelayToStart = 0;
unsigned long currentNShots = 0;
unsigned long currentTDuration = 0;
unsigned long NumberOfShotsSinceStart = 0;
unsigned long currentDelayBetweenShots = 0;
unsigned long currentTriggerDuration = 0;
unsigned long currentAutorefresh = 0;
int currentTimelapseAutoStart = timelapseAutoStart;

unsigned long secCounter = 0; // count the seconds since start of timelapse
unsigned long timeSlotCounter = 0; // count the timeslots in a second

int highlightLine = 2;
int lastDialogLine = 8;
unsigned long inactivityCounter = 0;
unsigned long previousMilliseconds = millis();
bool wifiON = SWITCH_WIFI_ON;
String fileList = "";
const String configFilename = "CONFIG";
String deleteFilename = "";
String execFilename = "";
String uploadFilename = "";
String uploadFileContent = "";
const String fileLineSeparator = "~";
bool SDCardExists = false;
String serialBuffer = "";
int serialBufferLinesCounter = 0;

String myIPStr = "";

enum lcdScreens { MAINSCREEN, CONFIGSCREEN, FILESCREEN };
lcdScreens actuScreen = MAINSCREEN;
enum lcdStates { LCDOFF, LCDON };
bool lcdIsON = true;
enum triggerModes { ON = 0, OFF = 1 };
triggerModes currentTriggerMode = OFF;
enum execModes          {  NONE,   ONESHOT,   TIMELAPSESTART,   TIMELAPSERUNNING,   TIMELAPSESTOP,   TIMETABLE,   EXECFILE,   DELETEFILE,   UPLOAD,   UPLOADOK,   UPLOADERROR,   WAITFORGPIO,   REFRESH,   RESET };
String execModesStr[] = { "NONE", "ONESHOT", "TIMELAPSESTART", "TIMELAPSERUNNING", "TIMELAPSESTOP", "TIMETABLE", "EXECFILE", "DELETEFILE", "UPLOAD", "UPLOADOK", "UPLOADERROR", "WAITFORGPIO", "REFRESH", "RESET" };
execModes currentExecMode = NONE;


// Create webserver on webServerPort
WiFiServer server(webServerPort);
WiFiClient client;

void setup()
{
  pinMode(microSDCardChipSelect, OUTPUT); // setup GPIO as CS for microSD card
  SDCardExists = SD.begin(microSDCardChipSelect);
  if (SDCardExists)
  {
    fileList = readFileList();
    loadConfigFromFile("/" + configFilename);
  }

  serialOut("", flushSerialBuffer); // flush the serialBuffer to serial port
  serialOut("------------------------------------", 2);
  serialOut("Start " + screenHeaderLine, 2);
  String sdCardInfo = (!SDCardExists) ? "No " : "";
  serialOut(sdCardInfo + "SD card found", 1);

  displaySetup();

  pinMode(triggerPin, OUTPUT); // setup GPIO as camera trigger
  if (autofocusPin > -1)
    pinMode(autofocusPin, OUTPUT); // setup GPIO for autofocus start function
  trigger(OFF);

  setupWiFi();

  checkAndProcessAutoStart();
}

void loop()
{
  processTimeSlots();
  checkAndProcessStartPin();
  processKeyboard();
  if (wifiON)
    processWebClient();
}

void setupWiFi()
{
  if (wifiON)
  {
    serialOut("Setup WIFI", 1);
    WiFi.mode(WIFI_AP); // AP mode for connections
    serialOut("setupWiFi.SSID: " + ssid, 99);
    serialOut("setupWiFi.Password: " + password, 99);
    WiFi.softAP(ssid.c_str(), password.c_str());
    server.begin();
    IPAddress ip = WiFi.softAPIP();
    myIPStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
    serialOut("IP address: " + myIPStr, 1);
  }
  else
    serialOut("WIFI is OFF", 1);
}

String getRequest(WiFiClient webclient)
{
  String retVal = "";
  if (webclient)
  {
    unsigned long timeout = millis() + 250;
    while (!webclient.available() && (millis() < timeout) )
      delay(1);
    retVal = webclient.readStringUntil('\r'); // Read the first line of the request
    serialOut("getRequest.Requestcontent: " + retVal, 99);
    webclient.flush();
  }
  return retVal;
}

void clientOutAndStop(String sHdr, String sRespo)
{
  client.print(sHdr); // Send the response to the client
  client.print(sRespo);
  client.stop(); // and stop the client
}

void processWebClient()
{
  String sResponse, sHeader, sParam = "";

  client = server.available(); // Check if a client has connected
  String sRequest = getRequest(client);
  if (sRequest != "") // There is a request
  {
    bool pathOK = checkPathAndGetParameters(sRequest, sParam); // get parameters from request, check path
    if (pathOK) // generate the html page
    {
      if (sParam != "")
      {
        extractParams(sParam);
        processMode();
      }
      sResponse = dispatchGenerateHTMLPage(delayToStart, numberOfShots, delayBetweenShots, triggerDuration);
      generateOKHTML(sResponse, sHeader);
    }
    else // 404 if error
      generateErrorHTML(sResponse, sHeader);
    clientOutAndStop(sHeader, sResponse);
  }
  else
    client.stop(); // stop client, if request is empty
}

void setAndDoProcessMode(execModes newExecMode)
{
  currentExecMode = newExecMode;
  processMode();
}

void setTimelapseAndTimetableValues()
{
  currentAutorefresh = autorefresh;
  secCounter = 0;
  timeSlotCounter = 0;
  currentDelayToStart = delayToStart;
  currentDelayBetweenShots = delayBetweenShots;
  currentTriggerDuration = triggerDuration;
  currentTDuration = triggerDuration;
  NumberOfShotsSinceStart = 0;
  currentNShots = numberOfShots;
  currentTriggerMode = OFF;
}

void processMode()
{
  serialOut("processMode.ExecMode: " + execModesStr[currentExecMode], 99);
  switch (currentExecMode)
  {
    case ONESHOT:
      {
        currentAutorefresh = 0;
        trigger(ON);
        currentDelayToStart = 0;
        currentNShots = 0;
        currentDelayBetweenShots = 0;
        currentTriggerDuration = 0;
        currentTDuration = 0;
        break;
      }
    case TIMELAPSESTOP:
      {
        currentAutorefresh = 0;
        currentNShots = 0;
        trigger(OFF);
        currentExecMode = NONE;
        timeTable = "";
        onDisplay(MAINSCREEN);
        break;
      }
    case RESET:
      {
        currentAutorefresh = 0;
        delayToStart = default_delayToStart;
        numberOfShots = default_numberOfShots;
        delayBetweenShots = default_delayBetweenShots;
        triggerDuration = default_triggerDuration;
        currentNShots = 0;
        currentTDuration = 0;
        trigger(OFF);
        currentExecMode = NONE;
        onDisplay(MAINSCREEN);
        break;
      }
    case WAITFORGPIO:
      {
        secCounter = 0;
        break;
      }
    case TIMELAPSESTART:
      {
        currentExecMode = TIMELAPSERUNNING;
        setTimelapseAndTimetableValues();
        doPerSecond(secCounter);
        break;
      }
    case TIMETABLE:
      {
        if (timeTable != "")
        {
          processTimeTable();
          setTimelapseAndTimetableValues();
          doPerSecond(secCounter);
        }
        else
          currentExecMode = TIMELAPSESTOP;
        break;
      }
    case UPLOAD:
      {
        if ((uploadFilename != "") && (uploadFileContent != "")) // something there to upload
          currentExecMode = (writeFile(uploadFilename, uploadFileContent)) ? UPLOADOK : UPLOADERROR;
        else
          currentExecMode = NONE;
        break;
      }
    case DELETEFILE:
      {
        if (deleteFilename != "")
          deleteFile(deleteFilename);
        currentExecMode = NONE;
        break;
      }
    case EXECFILE:
      {
        currentExecMode = NONE;
        if (execFilename != "")
        {
          String fileContent = readFile("/" + execFilename, timeTableSeparator);
          if (fileContent != "") // File is there
          {
            fileContent.toUpperCase();
            timeTable = fileContent;
            if (timeTable != "")
            {
              currentExecMode = TIMETABLE;
              processTimeTable();
              setTimelapseAndTimetableValues();
              doPerSecond(secCounter);
            }
          }
        }
        break;
      }
  }
  previousTimeSlotMillis = millis();
}

bool countSeconds()
{
  bool retVal = false;
  unsigned long currentTimeSlotMillis = millis();

  if ((currentTimeSlotMillis - previousTimeSlotMillis) > timeSlot) // 250 ms time slot
  {
    previousTimeSlotMillis = currentTimeSlotMillis;

    if ((currentTriggerMode == ON) && (currentTDuration == 0)) // only if trigger duration is 250ms
      switchTriggerOffAndCheckStatus();

    timeSlotCounter++; // Count the timeslots
    if (timeSlotCounter == timeSlotsPerSecond)
    {
      secCounter++; // Count the seconds
      timeSlotCounter = 0;
      retVal = true;
      if ((currentTriggerMode == ON) && (currentTDuration > 0))
      {
        currentTDuration--;
        if (currentTDuration == 0)
          switchTriggerOffAndCheckStatus();
      }
    }
  }
  return retVal;
}

void doPerSecond(unsigned long actuSecond)
{
  bool doNextTimeTableStep = false;
  if ((currentDelayToStart > 0) && (actuSecond > 0)) // Start delay
  {
    currentDelayToStart--;
    onDisplay(MAINSCREEN);
    if (currentDelayToStart == 0) // Delay is over
      doNextTimeTableStep = true;
  }
  if ((currentDelayBetweenShots > 0) && (currentDelayToStart == 0))
  {
    if ((actuSecond % currentDelayBetweenShots) == 0) // Delay between shots
    {
      if (currentNShots >= 1) // more than one shot timelapse?
      {
        if (currentTriggerMode == OFF)
          trigger(ON);
        currentNShots--;
        NumberOfShotsSinceStart++;
      }
      onDisplay(MAINSCREEN);
      if ((actuSecond == currentDelayBetweenShots) && (currentNShots <= 0))
        doNextTimeTableStep = true;
    }
  }
  if ((timeTable != "") && (doNextTimeTableStep))
    processMode();
}

void processTimeSlots()
{
  checkInactivity();
  if ((currentExecMode == NONE) || (currentExecMode == TIMELAPSESTOP) || (currentExecMode == WAITFORGPIO))
    return;
  if (countSeconds()) // Do something only every second starting with second 1
    doPerSecond(secCounter);
  if (currentExecMode == TIMETABLE)
  {
    timeTable.trim();
    if (timeTable == "")
      setAndDoProcessMode(RESET);
  }
}

// parse one timetable element
String parseElement(String element)
{
  delayToStart = 0; // Reset wait (delay) timer anyway
  numberOfShots = 0;
  delayBetweenShots = 0;
  trigger(OFF);
  String elementType;
  serialOut("parseElement.Element: " + element, 2);
  elementType = element[0];
  int xPosition = element.indexOf("X");
  if (elementType == "I") // wait till LOW to HIGH transition
  {
    int waitGPIOPin = atol(element.substring(1, element.length()).c_str());
    waitForGPIOTransition(waitGPIOPin);
  }
  else if (elementType == "L") // set GPIO to LOW
  {
    int lowGPIOPin = atol(element.substring(1, element.length()).c_str());
    pinMode(lowGPIOPin, OUTPUT);
    digitalWrite(lowGPIOPin, OFF);
  }
  else if (elementType == "H") // set GPIO to HIGH
  {
    int highGPIOPin = atol(element.substring(1, element.length()).c_str());
    pinMode(highGPIOPin, OUTPUT);
    digitalWrite(highGPIOPin, ON);
  }
  else if (elementType == "W") // set wait (=Delay) time
    delayToStart = atol(element.substring(1, element.length()).c_str());
  else if (elementType == "D") // Set trigger duration
    triggerDuration = atol(element.substring(1, element.length()).c_str());
  else if ((xPosition >= 1) && (element.length() >= 3)) // Number of shots and delay between shots in one element
  { // we need to expand this for one interval, e.g. 7x5 will be 5,6x5
    unsigned long nrOfShots = atol(element.substring(0, xPosition).c_str());
    delayBetweenShots = atol(element.substring(xPosition + 1, element.length()).c_str());
    String expandX = (nrOfShots > 1) ? String(nrOfShots - 1) + "X" + String(delayBetweenShots) + timeTableSeparator : "";
    timeTable = expandX + timeTable; // Replace X-Element by expanded string
    numberOfShots = 1;
  }
  else // delay after shot only
  {
    numberOfShots = 1;
    delayBetweenShots = atol(element.substring(0, element.length()).c_str());
  }
  return elementType;
}

// process timetable element by element
void processTimeTable()
{
  String elementType;
  String repeatTypes = "DILH"; // element types that will be followed by new processing immediatly
  do
  {
    serialOut("processTimeTable.Timetable: " + timeTable, 99);
    elementType = "";
    String element = extractElementFromList(timeTable, timeTableSeparator);
    if (element != "") // something is there
      elementType = parseElement(element);
    else
      timeTable = ""; // no more elements there
  }
  while ((elementType != "") && (repeatTypes.indexOf(elementType) >= 0)); // repeat immediately only if there are no timing elements
}

String extractElementFromList(String& elementList, String separator)
{
  String retVal = "";
  int idx = elementList.indexOf(separator); // Find end of next element
  if (idx >= 0)
  {
    if (idx > 0)
      retVal = elementList.substring(0, idx);
    elementList.remove(0, idx + 1); // remove element and separator from list
  }
  return retVal;
}

void switchTriggerOffAndCheckStatus()
{
  trigger(OFF);
  currentTDuration = currentTriggerDuration;
  if ((currentNShots <= 0) && (currentExecMode == TIMELAPSERUNNING))  // End of Timelapse mode
    currentExecMode = TIMELAPSESTOP;
}

void extractParams(String params)
{
  params[0] = ' ';
  params.trim();
  String originalParams = params;
  params.toUpperCase();
  if (params.length() > 0)
  {
    serialOut("URL parameters: " + originalParams, 2);
    if ((currentExecMode == TIMELAPSERUNNING) || (currentExecMode == REFRESH) || (currentExecMode == TIMETABLE)) // Only refresh and stop allowed in timelapse mode
    {
      if (params.indexOf("REFRESH") >= 0) // Refresh webGUI
        currentExecMode = REFRESH;
      if (params.indexOf("STOP") >= 0) // STOP timelapse
        currentExecMode = TIMELAPSESTOP;
    }
    else // blocked if timelapse running
    {
      if (params.indexOf("RESET") >= 0) // RESET form
        currentExecMode = RESET;
      else if (params.indexOf("SINGLE_SHOT") >= 0) // single shot
        currentExecMode = ONESHOT;
      else if ((params.indexOf("WAITFORGPIO") >= 0) && (startPin > -1)) // Wait for signal on input pin
      {
        currentExecMode = WAITFORGPIO;
        setTimelapseParams(params);
      }
      else if ((params.indexOf("DELAYTOSTART") >= 0) && (currentNShots <= 0)) // Switch to timelapse
      {
        currentExecMode = TIMELAPSESTART;
        setTimelapseParams(params);
      }
      else if ((params.indexOf("TIMETABLE") >= 0) && (currentNShots <= 0)) // Switch to timetable timelapse
      {
        currentExecMode = TIMETABLE;
        setTimelapseParams(params);
      }
      else if (params.indexOf("FILENAME") >= 0) // Filename for Upload to microSD card
      {
        currentExecMode = UPLOAD;
        setTimelapseParams(originalParams);
      }
      else if (params.indexOf("DELETEFILE") >= 0) // Filename for delete file from microSD card
      {
        currentExecMode = DELETEFILE;
        setTimelapseParams(params);
      }
      else if (params.indexOf("EXECFILE") >= 0) // Filename to execute a file from microSD card
      {
        currentExecMode = EXECFILE;
        setTimelapseParams(params);
      }
    }
  }
}

void setTimelapseParams(String params) // Set timelapse parameters for all modes
{
  String par = params + "& ";
  int idx = par.indexOf("&"); // Find element delimiter
  while (idx > 2)
  {
    String elem = par.substring(0, idx);
    par = par.substring(idx + 1, par.length());
    int idx2 = elem.indexOf("=");
    if (idx2 > 1)
    {
      String elemName = elem.substring(0, idx2); // Variable name
      elemName.toUpperCase();
      String elemValue = elem.substring(idx2 + 1, elem.length()); // Variable value
      serialOut("setTimelapseParams.Element: " + elemName, 99);
      serialOut("setTimelapseParams.Value: " + elemValue, 99);
      if (elemName == "DELAYTOSTART")
        delayToStart = elemValue.toInt();
      if (elemName == "NUMBEROFSHOTS")
        numberOfShots = elemValue.toInt();
      if (elemName == "DELAYBETWEENSHOTS")
        delayBetweenShots = elemValue.toInt();
      if (elemName == "TRIGGERDURATION")
        triggerDuration = elemValue.toInt();
      if (elemName == "TIMETABLE")
        timeTable = elemValue + timeTableSeparator;
      if (elemName == "FILENAME")
        uploadFilename = elemValue;
      if (elemName == "FILECONTENT")
        uploadFileContent = elemValue + fileLineSeparator;
      if (elemName == "DELETEFILE")
        deleteFilename = elemValue;
      if (elemName == "EXECFILE")
        execFilename = elemValue;
    }
    idx = par.indexOf("&");
  }
}

String dispatchGenerateHTMLPage(unsigned long sDelay, unsigned long nShots, unsigned long interval, unsigned long tDuration)
{
  if (fileList != "") // Show files on microSD card to select
    return generateFileListHTMLPage();
  else
    return generateHTMLPage(sDelay, nShots, interval, tDuration); // Standard timelapse screen;
}

String generateHTMLPageHeader(String refreshLine, int buttonWidth)
{
  return "<html><head><title>ANI Camera Remote</title>" + refreshLine + ""
         "<style>table, th, td { border: 0px solid black;} button, input[type=number], input[type=submit]{width:" + String(buttonWidth) + "px;height:28px;font-size:12pt;}</style></head><body>"
         "<font color=\"#000000\"><body bgcolor=\"#c0c0c0\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=yes\">"
         "<h2>" + prgTitle + " V" + prgVersion + "</h2>"
         "<table style=\"font-size:16px;\">";
}

String generateFileListHTMLPage()
{
  String localList = fileList;
  String retVal = generateHTMLPageHeader("", 180) + "<tr><td>Select a timetable file</td></tr>";
  while (true)
  {
    String filename = extractElementFromList(localList, timeTableSeparator);
    if (filename != "")
      retVal += "<tr><td><a href=?execfile=" + filename + "><button enabled>" + filename + "</button></a></td></tr>";
    else
      break;
  }
  retVal += "</table>";
  serialOut("generateFileListHTMLPage.HTML: " + retVal, 99);
  return retVal;
}

String generateHTMLPage(unsigned long sDelay, unsigned long nShots, unsigned long interval, unsigned long tDuration)
{
  String refreshLine, remainingTimeLine, waitForGPIOLine, waitForGPIOLineHint = "";
  String stateNotInTimelapse = "enabled";
  String buttonState = "disabled";
  bool showWaitForGPIOLineHint = true;
  // disable webGUI elements while in timelapse
  if ((((currentExecMode == TIMELAPSERUNNING) || (currentExecMode == REFRESH)) && (currentNShots > 0)) || (timeTable != ""))
  {
    buttonState = "enabled";
    stateNotInTimelapse = "disabled";
    showWaitForGPIOLineHint = false;
  }
  else if (currentExecMode == WAITFORGPIO)
    currentAutorefresh = autorefresh;
  else
  {
    currentAutorefresh = 0;
    if (currentExecMode == TIMETABLE)
      currentExecMode = RESET;
  }
  if (currentAutorefresh > 0)
  {
    refreshLine = "<meta http-equiv=\"refresh\" content=\"" + String(currentAutorefresh) + "; URL=http://" + myIPStr + "/\">";
    String remainingTime = getRemainingTimeStr(sDelay, nShots, interval, secCounter);
    remainingTimeLine = "<tr><td><FONT SIZE=-1>Remaining time:</td><td><FONT SIZE=-1>" + remainingTime + "</td></tr>";
  }
  if (startPin > -1) // webGUI elements für GPIO if defined
  {
    waitForGPIOLine = "<input type=submit value=\"Wait for GPIO" + String(startPin) + "\" " + stateNotInTimelapse + " name=WAITFORGPIO>";
    if (showWaitForGPIOLineHint)
      waitForGPIOLineHint = "<FONT SIZE=-2>GPIO" + String(startPin) + " input LOW starts timelapse<BR>";
  }
  // HTML here
  String retVal = generateHTMLPageHeader(refreshLine, 120) + ""
                  "<tr><td>Timelapse</td><td><a href=?function=RESET><button " + stateNotInTimelapse + ">Reset</button></a></td></tr>"
                  "<form method=GET>"
                  "<tr><td>Delay to start:</td><td><input " + stateNotInTimelapse + " id=delayToStart name=delayToStart type=number min=0 step=1 value=" + String(sDelay) + "> sec</td></tr>"
                  "<tr><td>Number of shots:</td><td><input " + stateNotInTimelapse + " id=numberOfShots name=numberOfShots type=number min=1 step=1 value=" + String(nShots) + "></td></tr>"
                  "<tr><td>Interval:</td><td><input " + stateNotInTimelapse + " id=delayBetweenShots name=delayBetweenShots type=number min=1 step=1 value=" + String(interval) + "> sec</td></tr>"
                  "<tr><td>Trigger duration (0=250ms):</td><td><input " + stateNotInTimelapse + " id=triggerDuration name=triggerDuration type=number min=0 step=1 value=" + String(tDuration) + "> sec</td></tr>"
                  "<tr><td></td><td></td></tr><tr><td></td><td></td></tr>"
                  "<tr><td>" + waitForGPIOLine + ""
                  "<input type=submit value=Start " + stateNotInTimelapse + " name=START></td>"
                  "</form>"
                  "<td><a href=?function=STOP><button " + buttonState + ">Stop</button></a></td></tr>"
                  "<tr><td></td><td></td></tr><tr><td></td><td></td></tr><tr><td></td><td></td></tr><tr><td></td><td></td></tr>"
                  "<tr><td><a href=?function=SINGLE_SHOT><button " + stateNotInTimelapse + ">One Shot</button></a></td><td><a href=?function=REFRESH><button " + buttonState + ">Refresh</button></a></td></tr>"
                  "<tr><td><FONT SIZE=-1>Remaining delay:</td><td><FONT SIZE=-1>" + String(currentDelayToStart) + " sec</td></tr>"
                  "<tr><td><FONT SIZE=-1>Remaining shots:</td><td><FONT SIZE=-1>" + String(currentNShots) + "</td></tr>"
                  "" + remainingTimeLine + ""
                  "</table></BR>"
                  "" + waitForGPIOLineHint + ""
                  "<FONT SIZE=-2>GPIO" + String(triggerPin) + " triggers camera shutter<BR>"
                  ;
  if (refreshLine != "")
    retVal += "<FONT SIZE=-2>Page will refresh every " + String(currentAutorefresh) +  " seconds<BR>";
  serialOut("generateHTMLPage.HTML: " + retVal, 99);
  return retVal;
}

void generateErrorHTML(String & sResp, String & sHead)
{
  sResp = "<html><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL was not found on this server.</p></body></html>";
  sHead = "HTTP/1.1 404 Not found\r\n"
          "Content-Length: " + String(sResp.length()) + "\r\n"
          "Content-Type: text/html\r\n"
          "Connection: close\r\n"
          "\r\n";
}

void generateOKHTML(String & sResp, String & sHead)
{
  sResp += "<FONT SIZE=-3>Andreas Niggemann " + String(versionMonth) + "/" + String(versionYear) + "<BR></body></html>";
  sHead = "HTTP/1.1 200 OK\r\n"
          "Content-Length: " + String(sResp.length()) + "\r\n"
          "Content-Type: text/html\r\n"
          "Connection: close\r\n"
          "\r\n";
}

bool checkPathAndGetParameters(String sReq, String & sPar)
{
  String sGetstart = "GET ", sPat = "";
  int iStart, iEndSpace, iEndQuest;
  iStart = sReq.indexOf(sGetstart);
  sPar = "";
  if (iStart >= 0)
  {
    iStart += +sGetstart.length();
    iEndSpace = sReq.indexOf(" ", iStart);
    iEndQuest = sReq.indexOf("?", iStart);

    if (iEndSpace > 0)
    {
      if (iEndQuest > 0)
      {
        sPat  = sReq.substring(iStart, iEndQuest);
        sPar = sReq.substring(iEndQuest, iEndSpace);
      }
      else
        sPat  = sReq.substring(iStart, iEndSpace);
    }
  }
  return sPat == "/";
}

void checkAndProcessStartPin()
{
  if (startPin > -1) // Input pin defined
  {
    pinMode(startPin, INPUT_PULLUP); // Setup GPIO input for start signal
    if (digitalRead(startPin) == LOW) // start signal
    {
      while (digitalRead(startPin) == LOW) // wait till change happens
        delay(100);
      setAndDoProcessMode(TIMELAPSESTART);
      serialOut("checkAndProcessStartPin.Timelapse start via GPIO" + String(startPin), 2);
    }
  }
}

void waitForGPIOTransition(int gpioPin)
{
  if (gpioPin > -1) // Input pin defined
  {
    pinMode(gpioPin, INPUT_PULLUP); // Setup GPIO input for wait input
    while (digitalRead(gpioPin) != LOW) // wait for signal LOW
      delay(100);
    while (digitalRead(gpioPin) == LOW) // wait till change happens
      delay(100);
    serialOut("LOW to HIGH transition on GPIO" + String(gpioPin), 2);
  }
}

void checkAndProcessAutoStart()
{
  if (currentTimelapseAutoStart == 1)
  {
    currentTimelapseAutoStart = 0;
    setAndDoProcessMode(TIMELAPSESTART);
    serialOut("checkAndProcessAutoStart.Autostart", 2);
  }
  else // Try Autostart file on SD Card
  {
    String fileContent = getAutoStartFile();
    if (fileContent != "") // Autostart file is there
    {
      fileContent.toUpperCase();
      timeTable = fileContent;
      setAndDoProcessMode(TIMETABLE);
    }
  }
}

String getRemainingTimeStr(unsigned long sDelay, unsigned long nShots, unsigned long interval, unsigned long secCounter)
{
  int seconds = ((nShots * interval) + sDelay) - secCounter;
  int days = seconds / 86400;
  int hours  = seconds % 86400 / 3600;
  int minutes  = seconds % 3600 / 60;
  seconds = seconds % 60;
  char timeFormat[100];
  sprintf(timeFormat, "%02d:%02d:%02d:%02d", days, hours, minutes, seconds);
  return String(timeFormat);
}

String getAutoStartFile()
{
  String retVal = "";
  if (SDCardExists)
  {
    retVal = readFile("/AUTO", timeTableSeparator);
    if (retVal == "") // AUTO was not there
      retVal = readFile("/AUTORUN", timeTableSeparator);
    if (retVal != "")
      serialOut("Found autostart file, do timetable with: " + retVal, 2);
  }
  return retVal;
}

String readFileList()
{
  File root = SD.open("/");
  String retVal = "";
  while (true)
  {
    File file = root.openNextFile();
    if (!file)
      break;
    if (!file.isDirectory())
    {
      String filename = file.name();
      serialOut("readFileList.Filename: " + filename, 99);
      if (filename.indexOf("/") == 0)
        filename.remove(0, 1);
      String upperCaseFilename = filename;
      upperCaseFilename.toUpperCase();
      if (upperCaseFilename != configFilename) // dont show configuration file
        retVal += filename + timeTableSeparator;
    }
  }
  if (retVal != "")
    serialOut("File list: " + retVal, 1);
  return retVal;
}

String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

boolean isValidNumber(String str)
{
  boolean isNum = false;
  for (byte i = 0; i < str.length(); i++)
  {
    isNum = isDigit(str.charAt(i)) || str.charAt(i) == '+' || str.charAt(i) == '.' || str.charAt(i) == '-';
    if (!isNum) return false;
  }
  return isNum;
}

void setConfiguration(String iniKey, String iniValue)
{
  unsigned long numValue = (isValidNumber(iniValue)) ? atol(iniValue.c_str()) : 0;
  if (iniKey == "SSID")
    ssid = iniValue;
  else if (iniKey == "PASSWORD")
    password = iniValue;
  else if (iniKey == "WEBSERVERPORT")
    webServerPort = numValue;
  else if (iniKey == "SCREENSAVERTIME")
    screenSaverTime = numValue;
  else if (iniKey == "SHUTTERSOUNDFREQUENCY")
    shutterSoundFrequency = numValue;
  else if (iniKey == "SERIALVERBOSITY")
    serialVerbosity = numValue;
  else if (iniKey == "TRIGGERPIN")
    triggerPin = numValue;
  else if (iniKey == "AUTOFOCUSPIN")
    autofocusPin = numValue;
  else if (iniKey == "STARTPIN")
    startPin = numValue;
  else if (iniKey == "DEFAULT_DELAYTOSTART")
    default_delayToStart = numValue;
  else if (iniKey == "DEFAULT_NUMBEROFSHOTS")
    default_numberOfShots = numValue;
  else if (iniKey == "DEFAULT_DELAYBETWEENSHOTS")
    default_delayBetweenShots = numValue;
  else if (iniKey == "DEFAULT_TRIGGERDURATION")
    default_triggerDuration = numValue;
  else if (iniKey == "DEFAULT_AUTOREFRESH")
    default_autorefresh = numValue;
  else if (iniKey == "TIMELAPSEAUTOSTART")
    timelapseAutoStart = numValue;
  else
    serialOut("setConfiguration ERROR Wrong key/value: " + iniKey + " = " + iniValue, 1);
}

void loadConfigFromFile(String filename)
{
  String lSeparator = "|";
  String configContent = readFile(filename, lSeparator);
  if (configContent != "") // CONFIG file was there and has something in it
  {
    configContent.remove(configContent.length() - 1); // cut off last lSeparator
    String iniLine, iniKey, iniValue = "";
    int idx = 0;
    do
    {
      iniLine = getValue(configContent, lSeparator[0], idx);
      if (iniLine != "")
      {
        iniLine += "= ";
        iniKey = getValue(iniLine, '=', 0);
        iniKey.trim();
        iniKey.toUpperCase();
        iniValue = getValue(iniLine, '=', 1);
        iniValue.trim();
        serialOut("Read Configuration: " + iniKey + " = " + iniValue, 1);
        setConfiguration(iniKey, iniValue);
      }
      idx++;
    }
    while (iniLine != "");
  }
}

String cleanStringFromDoubleSeparators(String content, String separator)
{
  while (content.indexOf(separator + separator) >= 0)
    content.replace(separator + separator, separator);
  return content;
}

String readFile(String filename, String lineSeparator)
{
  String fileContent = "";
  File actuFile;
  actuFile = SD.open(filename, FILE_READ);
  if (actuFile)
  {
    serialOut("Read file: " + filename, 1);
    char byteFromFile;
    while (actuFile.available())
    {
      char byteFromFile = actuFile.read();
      if (byteFromFile != '\r') // eliminate returns
      {
        byteFromFile = (byteFromFile == '\n') ? lineSeparator[0] : byteFromFile;
        fileContent = fileContent + byteFromFile;
      }
    }
    actuFile.close();
  }
  if (fileContent != "")
  {
    fileContent = cleanStringFromDoubleSeparators(fileContent, lineSeparator);
    serialOut("File content: " + fileContent, 2);
  }
  return fileContent;
}

String urldecode(String str)
{
  String encodedString = "";
  char c, code0, code1;
  for (int i = 0; i < str.length(); i++)
  {
    c = str.charAt(i);
    if (c == '+')
      encodedString += ' ';
    else if (c == '%')
    {
      i++;
      code0 = str.charAt(i);
      i++;
      code1 = str.charAt(i);
      c = (h2int(code0) << 4) | h2int(code1);
      encodedString += c;
    }
    else
      encodedString += c;
    yield();
  }
  return encodedString;
}

unsigned char h2int(char c)
{
  if (c >= '0' && c <= '9')
    return ((unsigned char)c - '0');
  if (c >= 'a' && c <= 'f')
    return ((unsigned char)c - 'a' + 10);
  if (c >= 'A' && c <= 'F')
    return ((unsigned char)c - 'A' + 10);
  return (0);
}

bool writeFile(String filename, String fileContent)
{
  bool retVal = false;
  File actuFile;
  String fname = (filename[0] == '/') ? filename : "/" + filename;
  actuFile = SD.open(fname, FILE_WRITE);
  if (actuFile)
  {
    fileContent = urldecode(fileContent);
    serialOut("Write file: " + fname, 2);
    serialOut("Write file content: " + fileContent, 99);
    int idx = 0;
    while (idx != -1)
    {
      idx = fileContent.indexOf(fileLineSeparator);
      if (idx > -1)
      {
        String actuLine =  (idx > 0) ? fileContent.substring(0, idx) : "";
        fileContent.remove(0, idx + 1);
        serialOut("Write line: " + actuLine, 99);
        actuFile.println(actuLine);
      }
    }
    actuFile.close();
    retVal = true;
  }
  return retVal;
}

void deleteFile(String NameOfFileToDelete)
{
  String rootName = "/" + NameOfFileToDelete;
  if (SD.exists(rootName))
    SD.remove(rootName);
  else if (SD.exists(NameOfFileToDelete))
    SD.remove(NameOfFileToDelete);
}

void serialWrite(String str, int itemVerbosityLevel)
{
  if (itemVerbosityLevel <= serialVerbosity)
  {
    Serial.println(str);
    Serial.flush();
  }
}

void serialOut(String str, int itemVerbosityLevel)
{
  String disableBuffer = "^";
  if (itemVerbosityLevel == 1000) // Flush the buffer to serial
  {
    if (serialVerbosity > 0)
    {
      Serial.begin(serialSpeed);
      for (int i = 0; i < serialBufferLinesCounter; i++)
      {
        int idx = serialBuffer.indexOf(fileLineSeparator);
        String bufferLine =  (idx > 0) ? serialBuffer.substring(0, idx) : "";
        serialBuffer.remove(0, idx + 1);
        int itemVLevel = atoi(bufferLine.substring(0, 2).c_str());
        bufferLine.remove(0, 2);
        serialWrite(bufferLine, itemVLevel);
      }
    }
    serialBuffer = disableBuffer;
  }
  else if (serialBuffer != disableBuffer) // Buffer the serial output
  {
    String vnumStr = String(itemVerbosityLevel);
    vnumStr = (itemVerbosityLevel < 10) ? "0" + vnumStr : vnumStr;
    serialBuffer += vnumStr + str + fileLineSeparator;
    serialBufferLinesCounter++;
  }
  else
    serialWrite(str, itemVerbosityLevel);
}

void trigger(triggerModes tMode)
{
  if (autofocusPin > -1)
    digitalWrite(autofocusPin, tMode);
  digitalWrite(triggerPin, tMode);
  shutterSound(tMode);
  currentTriggerMode = tMode;
  showTriggerState(tMode);
}

/*--------------------------------------------------
  Board specific methods
  Supported: ESP8266 and ESP32 (M5Stack)
  --------------------------------------------------*/
#ifdef ESP32
void displaySetup()
{
  serialOut("Setup M5Stack display", 2);
  M5.begin();
  onDisplay(MAINSCREEN);
}

void onDisplay(lcdScreens screen)
{
  int actuLine = 0;
  M5.Lcd.fillScreen(TFT_BLACK);
  M5.Lcd.setFreeFont(FSS9);
  outLCD(1, screenHeaderLine, TFT_BLUE);
  actuScreen = screen;
  switch (screen)
  {
    case MAINSCREEN:
      {
        outLCD(2, "Delay to Start: " + String(delayToStart), TFT_WHITE);
        outLCD(3, "Number of Shots: " + String(numberOfShots), TFT_WHITE);
        outLCD(4, "Interval: " + String(delayBetweenShots), TFT_WHITE);
        outLCD(5, "Trigger duration: " + String(triggerDuration), TFT_WHITE);
        if (startPin > -1)
          outLCD(6, "Wait for GPIO" + String(startPin), TFT_YELLOW);
        outLCD(7, "START", TFT_YELLOW);
        outLCD(8, "STOP", TFT_YELLOW);
        outLCD(9, "ONE SHOT", TFT_YELLOW);
        outLCD(10, "RESET", TFT_YELLOW);
        outLCD(11, "Rem. Delay: " + String(currentDelayToStart) + " Shots: " + String(currentNShots), TFT_GREEN);
        String remainingTimeStr = (currentNShots > 0) ? getRemainingTimeStr(currentDelayToStart, currentNShots, delayBetweenShots, 0) : getRemainingTimeStr(delayToStart, numberOfShots, delayBetweenShots, 0);
        outLCD(12, "Remaining Time: " + remainingTimeStr, TFT_GREEN);
        lastDialogLine = 10;
        break;
      }
    case CONFIGSCREEN:
      {
        outLCD(2, "WIFI ON", TFT_YELLOW);
        outLCD(3, "WIFI OFF", TFT_YELLOW);
        outLCD(4, "File Selector", TFT_YELLOW);
        outLCD(5, "EXIT Configuration", TFT_YELLOW);
        lastDialogLine = 5;
        break;
      }
    case FILESCREEN:
      {
        if (fileList != "") // at least one file on microSD card
        {
          String localList = fileList;
          int i;
          for (i = 0; i < 10; i++)
          {
            String filename = extractElementFromList(localList, timeTableSeparator);
            if (filename != "")
            {
              outLCD(i + 2, filename, TFT_YELLOW);
              serialOut("onDisplay.Display Filename: " + filename, 99);
            }
            else
              break;
          }
          outLCD(i + 2, "EXIT File List", TFT_YELLOW);
          lastDialogLine = i + 2;
        }
        break;
      }
  }
}

void changeValueOrExecFunction(int upDownFactor, lcdScreens screen, int lineNumber)
{
  highlightLine = 2;
  switch (screen)
  {
    case MAINSCREEN:
      {
        switch (lineNumber)
        {
          case 2: delayToStart = limitsUpDown(upDownFactor, delayToStart, 0, LONG_MAX); break;
          case 3: numberOfShots = limitsUpDown(upDownFactor, numberOfShots, 1, LONG_MAX) ; break;
          case 4: delayBetweenShots = limitsUpDown(upDownFactor, delayBetweenShots, 1, LONG_MAX); break;
          case 5: triggerDuration = limitsUpDown(upDownFactor, triggerDuration, 0, LONG_MAX); break;
          case 6: setAndDoProcessMode(WAITFORGPIO); break;
          case 7: setAndDoProcessMode(TIMELAPSESTART); break;
          case 8: setAndDoProcessMode(TIMELAPSESTOP); break;
          case 9: setAndDoProcessMode(ONESHOT); break;
          case 10: setAndDoProcessMode(RESET); break;
        }
        break;
      }
    case CONFIGSCREEN:
      {
        switch (lineNumber)
        {
          case 2: wifiON = true; setupWiFi(); break;
          case 3: ESP.restart(); break;
          case 4: onDisplay(FILESCREEN); break;
          case 5: onDisplay(MAINSCREEN); break;
        }
        break;
      }
    case FILESCREEN:
      {
        if (lineNumber == lastDialogLine)
          onDisplay(MAINSCREEN);
        else
        {
          String localList = fileList;
          String filename = "";
          for (int i = 2; i <= lineNumber; i++)
            filename = extractElementFromList(localList, timeTableSeparator);
          if (filename != "")
          {
            serialOut("Selected file: " + filename, 99);
            String fileContent = readFile("/" + filename, timeTableSeparator);
            if (fileContent != "") // file is there
            {
              fileContent.toUpperCase();
              timeTable = fileContent;
              setAndDoProcessMode(TIMETABLE);
            }
          }
          break;
        }
      }
  }
}

void outLCD(int lineNumber, String outStr, int textColor)
{
  const int lineFactor = 19;
  M5.Lcd.setTextColor(lineNumber == highlightLine ? TFT_RED : textColor);
  M5.Lcd.setCursor(0, lineNumber * lineFactor);
  M5.Lcd.printf(outStr.c_str());
}

void switchScreenOnOff(lcdStates dState)
{
  if (dState == LCDON)
  {
    M5.Lcd.writecommand(ILI9341_DISPON);
    M5.Lcd.setBrightness(50);
  }
  else
  {
    M5.Lcd.writecommand(ILI9341_DISPOFF);
    M5.Lcd.setBrightness(0);
  }
  lcdIsON = (dState == LCDON);
}

void checkInactivity()
{
  unsigned long currentMilliseconds = millis();
  if ((currentMilliseconds - previousMilliseconds) > 1000)
  {
    inactivityCounter++;
    if (inactivityCounter > screenSaverTime)
    {
      inactivityCounter = 0;
      switchScreenOnOff(LCDOFF);
    }
    previousMilliseconds = currentMilliseconds;
  }
}

long limitsUpDown(int upDownFactor, long number, long lowerLimit, int upperLimit)
{
  long retVal = number + upDownFactor;
  retVal = (retVal >= lowerLimit) ? retVal : lowerLimit;
  retVal = (retVal <= upperLimit) ? retVal : upperLimit;
  return retVal;
}

void processKeyboard()
{
  if (M5.BtnA.isPressed() || M5.BtnB.isPressed() || M5.BtnC.isPressed())
  {
    delay(100);
    inactivityCounter = 0;
    if (!lcdIsON) // LCD is in screensaver mode so we had to switch it on
      switchScreenOnOff(LCDON);
    else
    {
      int upDownFactor = M5.BtnA.isPressed() ? +1 : -1;
      if (M5.BtnA.isPressed() || M5.BtnB.isPressed())
      {
        int scaleFactor = 1;
        scaleFactor = M5.BtnC.isPressed() ? 100 : 1;
        changeValueOrExecFunction(upDownFactor * scaleFactor, actuScreen, highlightLine);
      }
      else if (M5.BtnC.wasPressed()) // Only button C: Move from line to line
        highlightLine = (highlightLine >= lastDialogLine) ? 2 : highlightLine + 1;
      else if (M5.BtnC.pressedFor(3000)) // Enter config menu after 3 seconds press on button C
        actuScreen = CONFIGSCREEN;
      onDisplay(actuScreen);
    }
  }
  M5.update();
}

void showTriggerState(triggerModes tMode)
{
  if (tMode == ON)
    M5.Lcd.fillScreen(TFT_WHITE);
  else
    onDisplay(actuScreen);
}

void shutterSound(triggerModes tMode)
{
  if (shutterSoundFrequency > 0)
  {
    M5.Speaker.setVolume(1);
    if (tMode == ON)
      M5.Speaker.tone(shutterSoundFrequency);
    else
      M5.Speaker.mute();
  }
}

#else
// Methods for ESP8266
void displaySetup() { }
void onDisplay(lcdScreens screen) { }
void checkInactivity() { }
void processKeyboard() { }
void showTriggerState(triggerModes tMode) { } // Maybe some LED in the future
void shutterSound(triggerModes tMode) { }
#endif
